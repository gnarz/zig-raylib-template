const std = @import("std");

// we build raylib here, but this does take care of everything we need to do for this.
const addRaylib = @import("raylib/src/build.zig").addRaylib;
const RLOptions = @import("raylib/src/build.zig").Options;

// setTarget() must have been called on step before calling this
fn addRaylibDependencies(step: *std.build.LibExeObjStep, raylib: *std.build.LibExeObjStep) void {
    step.addIncludePath(std.Build.LazyPath{ .path = "raylib/zig-out/include" });

    // raylib's build.zig file specifies all libraries this executable must be
    // linked with, so let's copy them from there.
    for (raylib.link_objects.items) |o| {
        if (o == .system_lib) {
            step.linkSystemLibrary(o.system_lib.name);
        }
    }
    if (step.target.isWindows()) {
        step.addObjectFile(std.Build.LazyPath{ .path = "zig-out/lib/raylib.lib" });
    } else {
        step.addObjectFile(std.Build.LazyPath{ .path = "zig-out/lib/libraylib.a" });
    }
}

pub fn build(b: *std.build.Builder) void {
    const optopt = b.standardOptimizeOption(.{});
    const target = b.standardTargetOptions(.{});
    var rloptions = RLOptions{
        .raudio = true,
        .rmodels = true,
        .rshapes = true,
        .rtext = true,
        .rtextures = true,
        .raygui = false,
        .platform_drm = false,
    };

    const raylib = addRaylib(b, target, optopt, rloptions);
    raylib.installHeader("raylib/src/raylib.h", "raylib.h");
    raylib.installHeader("raylib/src/raymath.h", "raymath.h");
    raylib.installHeader("raylib/src/rlgl.h", "rlgl.h");

    if (rloptions.raygui) {
        raylib.installHeader("raygui/src/raygui.h", "raygui.h");
    }

    b.installArtifact(raylib);

    const exe = b.addExecutable(.{
        .name = "lmc",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optopt,
    });
    exe.linkLibC();
    addRaylibDependencies(exe, raylib);
    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const exe_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optopt,
    });
    addRaylibDependencies(exe_tests, raylib);

    const run_exe_tests = b.addRunArtifact(exe_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_exe_tests.step);
}
