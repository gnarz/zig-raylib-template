# zig-raylib-template

a simple template to get started with raylib and zig. It is modeled after this one: https://github.com/raysan5/raylib-game-template

The resources are copied from the original repository.

Note that raylib is included as a submodule, so you either need to clone this repo with --recursive, or do a git submodule update --init
