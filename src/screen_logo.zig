const ray = @import("raylib.zig");
const screen = @import("screen.zig");

pub fn load() screen.Screen {
    const vtable = screen.Screen.VTable{
        .init = init,
        .deinit = deinit,
        .update = update,
        .draw = draw,
        .isFinished = isFinished,
    };

    return screen.init(&vtable);
}

var done: bool = false;

const logo_text = "Logo Screen";
var logo_text_size: i32 = 40;
var logo_width: i32 = 0;
var logo_height: i32 = 0;

fn init() void {
    var logo_size = ray.MeasureTextEx(ray.GetFontDefault(), logo_text, @floatFromInt(logo_text_size), 1);
    logo_width = @intFromFloat(logo_size.x);
    logo_height = @intFromFloat(logo_size.y);
    done = false;
}

fn deinit() void {}

var alpha: f32 = 0.0;
var alpha_going_up = true;

fn update() void {
    if (alpha_going_up) {
        alpha += 0.01;
        if (alpha > 1.0) {
            alpha_going_up = false;
            alpha = 1.0;
        }
    } else {
        alpha -= 0.01;
        if (alpha < 0.0) {
            done = true;
            alpha = 0.0;
        }
    }
}

fn draw() void {
    ray.ClearBackground(ray.RAYWHITE);
    var xpos = (ray.GetScreenWidth() - logo_width) >> 1;
    var ypos = (ray.GetScreenHeight() - logo_height) >> 1;
    ray.DrawText(logo_text, xpos, ypos, logo_text_size, ray.Fade(ray.BLACK, alpha));
}

fn isFinished() bool {
    return done;
}

test "basic compilation" {
    @import("std").testing.refAllDecls(@This());
}
