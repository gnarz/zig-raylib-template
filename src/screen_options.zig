const ray = @import("raylib.zig");
const screen = @import("screen.zig");

pub fn load() screen.Screen {
    const vtable = screen.Screen.VTable{
        .init = init,
        .deinit = deinit,
        .update = update,
        .draw = draw,
        .isFinished = isFinished,
    };

    return screen.init(&vtable);
}

var done: bool = false;

const options_text = "Options Screen";
var options_text_size: i32 = 40;
var options_width: i32 = 0;
var options_height: i32 = 0;

fn init() void {
    var size = ray.MeasureTextEx(ray.GetFontDefault(), options_text, @floatFromInt(options_text_size), 1);
    options_width = @intFromFloat(size.x);
    options_height = @intFromFloat(size.y);
    done = false;
}

fn deinit() void {}

fn update() void {
    if (ray.GetKeyPressed() != 0) {
        done = true;
    }
}

fn draw() void {
    ray.ClearBackground(ray.RAYWHITE);
    var xpos = (ray.GetScreenWidth() - options_width) >> 1;
    var ypos = ((ray.GetScreenHeight() - options_height) >> 1) - options_height;
    ray.DrawText(options_text, xpos, ypos, options_text_size, ray.BLUE);
}

fn isFinished() bool {
    return done;
}

test "basic compilation" {
    @import("std").testing.refAllDecls(@This());
}
