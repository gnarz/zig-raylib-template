const ray = @import("raylib.zig");
const screen = @import("screen.zig");

pub fn load() screen.Screen {
    const vtable = screen.Screen.VTable{
        .init = init,
        .deinit = deinit,
        .update = update,
        .draw = draw,
        .isFinished = isFinished,
    };

    return screen.init(&vtable);
}

var done: bool = false;

const ending_text = "Ending Screen";
var ending_text_size: i32 = 40;
var ending_width: i32 = 0;
var ending_height: i32 = 0;

fn init() void {
    var size = ray.MeasureTextEx(ray.GetFontDefault(), ending_text, @floatFromInt(ending_text_size), 1);
    ending_width = @intFromFloat(size.x);
    ending_height = @intFromFloat(size.y);
    done = false;
}

fn deinit() void {}

fn update() void {
    if (ray.GetKeyPressed() != 0) {
        done = true;
    }
}

fn draw() void {
    ray.ClearBackground(ray.RAYWHITE);
    var xpos = (ray.GetScreenWidth() - ending_width) >> 1;
    var ypos = ((ray.GetScreenHeight() - ending_height) >> 1) - ending_height;
    ray.DrawText(ending_text, xpos, ypos, ending_text_size, ray.RED);
}

fn isFinished() bool {
    return done;
}

test "basic compilation" {
    @import("std").testing.refAllDecls(@This());
}
