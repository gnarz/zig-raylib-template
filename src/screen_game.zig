const ray = @import("raylib.zig");
const screen = @import("screen.zig");
const smgr = @import("screenmgr.zig");
const main = @import("main.zig");

pub fn load() screen.Screen {
    const vtable = screen.Screen.VTable{
        .init = init,
        .deinit = deinit,
        .update = update,
        .draw = draw,
        .isFinished = isFinished,
    };

    return screen.init(&vtable);
}

var done: bool = false;

const game_text = "Game Screen";
var game_text_size: i32 = 40;
var game_width: i32 = 0;
var game_height: i32 = 0;
var game_posx: i32 = 0;
var game_posy: i32 = 0;

var paused = false;

pub fn unpause() void {
    paused = false;
}

fn init() void {
    var size = ray.MeasureTextEx(ray.GetFontDefault(), game_text, @floatFromInt(game_text_size), 1);
    game_width = @intFromFloat(size.x);
    game_height = @intFromFloat(size.y);
    game_posx = (ray.GetScreenWidth() - game_width) >> 1;
    game_posy = (ray.GetScreenHeight() - game_height) >> 1;
    done = false;
}

fn deinit() void {}

fn update() void {
    if (paused) return;

    if (ray.IsKeyDown(ray.KEY_UP) and game_posy > 0) {
        game_posy -= 1;
    }
    if (ray.IsKeyDown(ray.KEY_DOWN) and game_posy < ray.GetScreenHeight()) {
        game_posy += 1;
    }
    if (ray.IsKeyDown(ray.KEY_LEFT) and game_posx > 0) {
        game_posx -= 1;
    }
    if (ray.IsKeyDown(ray.KEY_RIGHT) and game_posx < ray.GetScreenWidth()) {
        game_posx += 1;
    }
    if (ray.IsKeyDown(ray.KEY_M)) {
        paused = true;
        smgr.pushScreen(main.screen_ingamemenu) catch unreachable;
    }
    if (ray.IsKeyPressed(ray.KEY_SPACE)) {
        done = true;
    }
}

fn draw() void {
    ray.ClearBackground(ray.RAYWHITE);
    ray.DrawText(game_text, game_posx, game_posy - (game_height >> 1), game_text_size, ray.GREEN);
}

fn isFinished() bool {
    return done;
}

test "basic compilation" {
    @import("std").testing.refAllDecls(@This());
}
