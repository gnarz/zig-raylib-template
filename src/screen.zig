/// Screen struct. Screens are singletons, sharing a common interface. Thus there
/// is no state stored in the struct, but rather in the implementation.
//
pub const Screen = struct {
    pub const VTable = struct {
        init: *const fn () void,
        deinit: *const fn () void,
        update: *const fn () void,
        draw: *const fn () void,
        isFinished: *const fn () bool,
        getValue: *const fn () i32 = defaultGetValue,
    };

    vtable: *const VTable,

    fn defaultGetValue() i32 {
        return 0;
    }

    pub fn init(self: *Screen) void {
        return self.vtable.init();
    }

    pub fn deinit(self: *Screen) void {
        return self.vtable.deinit();
    }

    pub fn update(self: *Screen) void {
        return self.vtable.update();
    }
    pub fn draw(self: *Screen) void {
        return self.vtable.draw();
    }
    pub fn isFinished(self: *Screen) bool {
        return self.vtable.isFinished();
    }
    pub fn getValue(self: *Screen) i32 {
        return self.vtable.getValue();
    }
};

pub fn init(vtable: *const Screen.VTable) Screen {
    return Screen{
        .vtable = vtable,
    };
}

test "basic compilation" {
    @import("std").testing.refAllDecls(@This());
    @import("std").testing.refAllDecls(Screen);
}
