const ray = @import("raylib.zig");
const screen = @import("screen.zig");
const game = @import("screen_game.zig");

pub fn load() screen.Screen {
    const vtable = screen.Screen.VTable{
        .init = init,
        .deinit = deinit,
        .update = update,
        .draw = draw,
        .isFinished = isFinished,
    };

    return screen.init(&vtable);
}

var done: bool = false;

const igm_text = "Ingame Menu Screen";
var igm_text_size: i32 = 40;
var igm_width: i32 = 0;
var igm_height: i32 = 0;

fn init() void {
    var size = ray.MeasureTextEx(ray.GetFontDefault(), igm_text, @floatFromInt(igm_text_size), 1);
    igm_width = @intFromFloat(size.x);
    igm_height = @intFromFloat(size.y);
    done = false;
}

fn deinit() void {
    game.unpause();
}

fn update() void {
    if (ray.GetKeyPressed() != 0) {
        done = true;
    }
}

fn draw() void {
    var width = ray.GetScreenWidth();
    var height = ray.GetScreenHeight();
    ray.DrawRectangle(60, 30, width - 120, height - 60, ray.YELLOW);
    ray.DrawRectangleLines(60, 30, width - 120, height - 60, ray.BLACK);
    var xpos = (width - igm_width) >> 1;
    var ypos = ((height - igm_height) >> 1) - igm_height;
    ray.DrawText(igm_text, xpos, ypos, igm_text_size, ray.BLUE);
}

fn isFinished() bool {
    return done;
}

test "basic compilation" {
    @import("std").testing.refAllDecls(@This());
}
