/// a simple screen manager. It stores all used screens, and their relations. Switching
/// between screens is done automatically when isFinished() for the current screen
/// returns true. Screens may be pushed atop other screens in order to allow for
/// transient menus and stuff.
//
const std = @import("std");
const ray = @import("raylib.zig");
const Screen = @import("screen.zig").Screen;
const Allocator = std.mem.Allocator;

const Next = struct {
    sid: usize,
    val: i32,
    nid: usize,
};

var allocator: std.mem.Allocator = undefined;
var screens: std.ArrayList(Screen) = undefined;
var nexts: std.ArrayList(Next) = undefined;
var stack: std.ArrayList(usize) = undefined;
var should_quit: bool = false;

inline fn numScreens() usize {
    return screens.items.len;
}

inline fn stackSize() usize {
    return stack.items.len;
}

fn clearStack() void {
    while (stack.popOrNull()) |s| {
        screens.items[s].deinit();
    }
}

fn topScreen() usize {
    std.debug.assert(stack.items.len > 0);
    return stack.items[stack.items.len - 1];
}

/// initialize the screen manager
pub fn init(a: std.mem.Allocator) Allocator.Error!void {
    allocator = a;
    screens = std.ArrayList(Screen).init(a);
    nexts = std.ArrayList(Next).init(a);
    stack = try std.ArrayList(usize).initCapacity(a, 1);
}

/// deinitialize the screen manager and free allocated memory
pub fn deinit() void {
    clearStack();
    stack.deinit();
    nexts.deinit();
    screens.deinit();
}

/// add a screen to manage, return its id
pub fn addScreen(s: Screen) Allocator.Error!usize {
    try screens.append(s);
    return screens.items.len - 1;
}

/// specify which screen follows screen with id s, if getValue() on screen s
/// returns v. This allows for multiple followup screens depending on v.
pub fn setNext(s: usize, v: i32, n: usize) Allocator.Error!void {
    std.debug.assert(s < numScreens() and n < numScreens());
    try nexts.append(.{ .sid = s, .val = v, .nid = n });
}

/// close the current screen and open the next one, as specified with setNext().
/// If the current screen is a pushed screen, it will be closed and the previous
/// screen will be resumed. If the vaule returned by the getValue() method of
/// the current screen is negative, then should_quit is set to true causing
/// ScreenManager.shouldQuit() to return true.
pub fn nextScreen() void {
    // TODO FEATURE: transitions
    std.debug.assert(stackSize() > 0);
    var current = stack.pop();
    var next = current;
    std.debug.assert(current < numScreens());
    var val = screens.items[current].getValue();
    if (val < 0) {
        should_quit = true;
        return;
    }
    if (stackSize() == 0) {
        for (nexts.items) |n| {
            if (n.sid == current and n.val == val) {
                next = n.nid;
            }
        }
        if (next != current) {
            screens.items[current].deinit();
            gotoScreen(next);
        }
    } else screens.items[current].deinit();
}

/// empty the screen stack and open a new screen.
pub fn gotoScreen(s: usize) void {
    clearStack();
    // stack was initialized to hold at least 1 element, so this can not fail
    pushScreen(s) catch unreachable;
}

/// push a screen to the screen stack
pub fn pushScreen(s: usize) Allocator.Error!void {
    std.debug.assert(s < numScreens());
    screens.items[s].init();
    try stack.append(s);
}

/// update all currently active screens, from bottom to top. If the topmost screen
/// is finished, switch to the next screen
pub fn update() void {
    for (stack.items) |s| {
        screens.items[s].update();
    }
    if (screens.items[topScreen()].isFinished()) nextScreen();
}

/// draw all currently active screens
pub fn draw() void {
    for (stack.items) |s| {
        screens.items[s].draw();
    }
}

/// return true when the game loop should exit, false otherwise
pub fn shouldQuit() bool {
    return ray.WindowShouldClose() or should_quit;
}

test "check if everything passes semantic analysis" {
    std.testing.refAllDecls(@This());
}
