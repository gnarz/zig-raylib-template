const ray = @import("raylib.zig");
const screen = @import("screen.zig");
const rmgr = @import("resourcemgr.zig");
const main = @import("main.zig");

pub fn load() screen.Screen {
    const vtable = screen.Screen.VTable{
        .init = init,
        .deinit = deinit,
        .update = update,
        .draw = draw,
        .isFinished = isFinished,
        .getValue = getValue,
    };

    return screen.init(&vtable);
}

var done: bool = false;
var result: i32 = 0;

const title_text1 = "Title Screen";
var title_text1_size: i32 = 40;
var title_width1: i32 = 0;
var title_height1: i32 = 0;

const title_text2 = "Press 1 for options, 2 for game";
var title_text2_size: i32 = 20;
var title_width2: i32 = 0;
var title_height2: i32 = 0;

fn init() void {
    var size = ray.MeasureTextEx(ray.GetFontDefault(), title_text1, @floatFromInt(title_text1_size), 1);
    title_width1 = @intFromFloat(size.x);
    title_height1 = @intFromFloat(size.y);
    size = ray.MeasureTextEx(ray.GetFontDefault(), title_text2, @floatFromInt(title_text2_size), 1);
    title_width2 = @intFromFloat(size.x);
    title_height2 = @intFromFloat(size.y);
    done = false;
    result = 0;
}

fn deinit() void {}

fn update() void {
    if (ray.IsKeyPressed(ray.KEY_ONE)) {
        done = true; // OPTIONS
        ray.PlaySound(main.fx_coin);
    } else if (ray.IsKeyPressed(ray.KEY_TWO)) {
        result = 1; // GAME
        done = true;
        ray.PlaySound(main.fx_coin);
    }
}

fn draw() void {
    ray.ClearBackground(ray.RAYWHITE);
    var xpos = (ray.GetScreenWidth() - title_width1) >> 1;
    var ypos = ((ray.GetScreenHeight() - title_height1) >> 1) - title_height1;
    ray.DrawText(title_text1, xpos, ypos, title_text1_size, ray.BLACK);

    xpos = (ray.GetScreenWidth() - title_width2) >> 1;
    ypos = (ray.GetScreenHeight() - title_height2) >> 1;
    ray.DrawText(title_text2, xpos, ypos, title_text2_size, ray.RED);
}

fn isFinished() bool {
    return done;
}

fn getValue() i32 {
    return result;
}

test "basic compilation" {
    @import("std").testing.refAllDecls(@This());
}
