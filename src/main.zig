const std = @import("std");
const ray = @import("raylib.zig");
const smgr = @import("screenmgr.zig");
const rmgr = @import("resourcemgr.zig");

const allocator = std.heap.c_allocator;

// transient screens
pub var screen_ingamemenu: usize = 0;

// resources
pub var font: ray.Font = undefined;
pub var music: ray.Music = undefined;
pub var fx_coin: ray.Sound = undefined;

pub fn main() !void {
    const screenWidth = 800;
    const screenHeight = 450;

    ray.InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");
    defer ray.CloseWindow();
    ray.InitAudioDevice();

    rmgr.init(allocator);
    defer rmgr.deinit();
    try setupResources();

    ray.SetMusicVolume(music, 1.0);
    ray.PlayMusicStream(music);
    ray.SetSoundVolume(fx_coin, 1.0);

    try smgr.init(allocator);
    defer smgr.deinit();
    try setupScreens();

    ray.SetTargetFPS(60);

    while (!ray.WindowShouldClose()) {
        ray.UpdateMusicStream(music);

        ray.BeginDrawing();
        defer ray.EndDrawing();

        smgr.update();
        smgr.draw();
    }
}

fn setupResources() !void {
    font = try rmgr.loadFont("resources/mecha.png");
    music = try rmgr.loadMusic("resources/ambient.ogg");
    fx_coin = try rmgr.loadSound("resources/coin.wav");
}

fn setupScreens() !void {
    var screen_logo = try smgr.addScreen(@import("screen_logo.zig").load());
    var screen_title = try smgr.addScreen(@import("screen_title.zig").load());
    var screen_options = try smgr.addScreen(@import("screen_options.zig").load());
    var screen_game = try smgr.addScreen(@import("screen_game.zig").load());
    var screen_ending = try smgr.addScreen(@import("screen_ending.zig").load());
    // pushed screen, where the underlying screen is still drawn
    screen_ingamemenu = try smgr.addScreen(@import("screen_ingamemenu.zig").load());

    try smgr.setNext(screen_logo, 0, screen_title);
    try smgr.setNext(screen_title, 0, screen_options);
    try smgr.setNext(screen_title, 1, screen_game);
    try smgr.setNext(screen_options, 0, screen_title);
    try smgr.setNext(screen_game, 0, screen_ending);
    try smgr.setNext(screen_ending, 0, screen_title);

    smgr.gotoScreen(screen_logo);
}

test "basic compilation" {
    std.testing.refAllDecls(@This());
}
