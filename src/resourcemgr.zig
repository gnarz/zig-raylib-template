/// a simple resource manager. All resources are allocated in one place, and also
/// released together when the resource manager is deinitialized.
/// The ownership of all resources remains with the resource manager at all times!
/// The raylib resource structs only carry pointers to allocated memory, so they
/// can be copied without problems as long as you remember who owns the data.
//
const std = @import("std");
const Allocator = std.mem.Allocator;
const ray = @import("raylib.zig");
const Font = ray.Font;
const Music = ray.Music;
const Sound = ray.Sound;
const Shader = ray.Shader;
const Image = ray.Image;
const Texture = ray.Texture;
const Model = ray.Model;
// more stuff...

const Resource = union(enum) {
    font: Font,
    music: Music,
    sound: Sound,
    shader: Shader,
    image: Image,
    texture: Texture,
    model: Model,
};

var allocator: std.mem.Allocator = undefined;
var resources: std.ArrayList(Resource) = undefined;

pub fn init(a: std.mem.Allocator) void {
    allocator = a;
    resources = std.ArrayList(Resource).init(a);
}

pub fn deinit() void {
    for (resources.items) |res| {
        switch (res) {
            .font => ray.UnloadFont(res.font),
            .music => ray.UnloadMusicStream(res.music),
            .sound => ray.UnloadSound(res.sound),
            .shader => ray.UnloadShader(res.shader),
            .image => ray.UnloadImage(res.image),
            .texture => ray.UnloadTexture(res.texture),
            .model => ray.UnloadModel(res.model),
        }
    }
    resources.deinit();
}

pub fn loadFont(filename: [*c]const u8) Allocator.Error!Font {
    var res = try resources.addOne();
    res.* = .{ .font = ray.LoadFont(filename) };
    return res.*.font;
}

pub fn loadMusic(filename: [*c]const u8) Allocator.Error!Music {
    var res = try resources.addOne();
    res.* = .{ .music = ray.LoadMusicStream(filename) };
    return res.*.music;
}

pub fn loadSound(filename: [*c]const u8) Allocator.Error!Sound {
    var res = try resources.addOne();
    res.* = .{ .sound = ray.LoadSound(filename) };
    return res.*.sound;
}

pub fn loadShader(vshader: [*c]const u8, fshader: [*c]const u8) Allocator.Error!Shader {
    var res = try resources.addOne();
    res.* = .{ .shader = ray.LoadShader(vshader, fshader) };
    return res.*.shader;
}

pub fn loadImage(filename: [*c]const u8) Allocator.Error!Image {
    var res = try resources.addOne();
    res.* = .{ .image = ray.LoadImage(filename) };
    return res.*.image;
}

pub fn loadTexture(filename: [*c]const u8) Allocator.Error!Texture {
    var res = try resources.addOne();
    res.* = .{ .texture = ray.LoadTexture(filename) };
    return res.*.texture;
}

pub fn loadModel(filename: [*c]const u8) Allocator.Error!Model {
    var res = try resources.addOne();
    res.* = .{ .model = ray.LoadModel(filename) };
    return res.*.model;
}

test "basic compilation" {
    std.testing.refAllDecls(@This());
}
